module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: true,
  preset: '@vue/cli-plugin-unit-jest',
  transform: {
    '^.+\\.vue$': 'vue-jest',
  },
};
