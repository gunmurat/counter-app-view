import { mount, flushPromises } from '@vue/test-utils';
import axios from 'axios';
import Home from '../../src/components/Home.vue';
import * as service from '../../src/service/counter_service';

describe('Home Components Render Tests', () => {
  // This test ensures that the Home component renders the correct markup
  // when a user visits the page for the first time
  // The test will fail if the markup is not what we expect
  // The test will pass if the markup is what we expect

  test('should vue instance defined', () => {
    // This test ensures that the Home component renders and is a Vue instance

    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    // isVisible is a helper function that checks if the component is visible
    expect(wrapper.isVisible()).toBe(true);
  });

  test('should render + button', () => {
    // This test ensures that the Increment button is rendered with the correct markup

    // This is the text that we expect to see on the button
    const incrementButtonText = '+';

    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    // If the text on the button is incrementButtonText, the test will pass
    expect(wrapper.find('.increment').text()).toBe(incrementButtonText);
  });

  test('should render - button', () => {
    // This test ensures that the Decrement button is rendered with the correct markup

    // This is the text that we expect to see on the button
    const decrementButtonText = '-';

    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    // If the text on the button is decrementButtonText, the test will pass
    expect(wrapper.find('.decrement').text()).toBe(decrementButtonText);
  });

  test('should render reset button', () => {
    // This test ensures that the Reset button is rendered with the correct markup

    // This is the text that we expect to see on the button
    const resetButtonText = 'reset';

    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    // If the text on the button is resetButtonText, the test will pass
    expect(wrapper.find('.reset').text()).toBe(resetButtonText);
  });

  test('should render counter', async () => {
    // This test ensures that the Counter is rendered with the correct markup

    // This is the text that we expect to see on the counter
    const mockedData = 0;
    const mockedDataString = mockedData.toString();
    const spy = jest.spyOn(service, 'getCounter').mockImplementation(() => {
      return mockedData;
    });

    await flushPromises();

    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    await wrapper.vm.getCounterImpl();

    // If the text on the counter is counterText, the test will pass
    expect(wrapper.find('span').text()).toBe(mockedDataString);
  });
});

describe('Home Components Function Call Tests', () => {
  // This test ensures that the Home component calls the correct functions
  // when the user interacts with the buttons
  // The test will fail if the functions are not called correctly
  // The test will pass if the functions are called correctly

  test('should increment method call when click increment button', () => {
    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    // This is a helper function that will call the increment method on the component
    wrapper.vm.incrementCounterImpl = jest.fn();

    // Find the increment button with wrapper.find and click it
    const incrementButton = wrapper.find('.increment');
    incrementButton.trigger('click');

    // If the increment method was called
    // when the increment button was clicked, the test will pass
    expect(wrapper.vm.incrementCounterImpl).toHaveBeenCalled();

    wrapper.vm.incrementCounterImpl.mockRestore();
  });

  test('should increment method call when click decrement button', () => {
    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);

    // This is a helper function that will call the decrement method on the component
    wrapper.vm.decrementCounterImpl = jest.fn();

    // Find the decrement button with wrapper.find and click it
    const incrementButton = wrapper.find('.decrement');
    incrementButton.trigger('click');

    // If the decrement method was called
    // when the decrement button was clicked, the test will pass
    expect(wrapper.vm.decrementCounterImpl).toHaveBeenCalled();
    wrapper.vm.decrementCounterImpl.mockRestore();
  });

  test('should reset method call when click reset button', () => {
    // shallowMount is a helper function that mounts the component and returns a wrapper object
    const wrapper = mount(Home);
    // This is a helper function that will call the reset method on the component
    wrapper.vm.resetCounterImpl = jest.fn();

    // Find the reset button with wrapper.find and click it
    const reset = wrapper.find('.reset');
    reset.trigger('click');

    // If the reset method was called
    // when the reset button was clicked, the test will pass
    expect(wrapper.vm.resetCounterImpl).toHaveBeenCalled();
    wrapper.vm.resetCounterImpl.mockRestore();
  });
});

describe('Counter Service Unit Tests', () => {
  // This test ensures that the Counter service is called correctly
  // The test will fail if the service is not called correctly
  // The test will pass if the service is called correctly
  test('should validate number correctly', () => {
    const wrapper = mount(Home);
    // Validate that the number is valid in the validateNumber function
    // If the number is valid, the test will pass
    // INCREASABLE = 0
    // DECREASABLE = 1
    // BOTH = 2
    // UNDEFINED = 3
    expect(wrapper.vm.validateNumber(1)).toBe(2);
    expect(wrapper.vm.validateNumber(0)).toBe(0);
    expect(wrapper.vm.validateNumber(9)).toBe(1);
  });

  test('should get counter correctly', async () => {
    const mocketInitData = 0;
    const spy = jest.spyOn(service, 'getCounter').mockImplementation(() => {
      return mocketInitData;
    });

    const wrapper = mount(Home);
    await flushPromises();
    await wrapper.vm.getCounterImpl();
    expect(spy).toHaveBeenCalled();
    expect(wrapper.vm.counter).toBe(mocketInitData);

    spy.mockRestore();
  });

  test('should increment counter correctly', async () => {
    const mockedData = 3;
    const mocketInitData = 0;
    const spyInc = jest.spyOn(service, 'getCounter').mockImplementation(() => {
      return mocketInitData;
    });

    const spy = jest
      .spyOn(service, 'incrementCounter')
      .mockImplementation(() => {
        return mockedData;
      });

    const wrapper = mount(Home);
    await flushPromises();
    await wrapper.vm.incrementCounterImpl();
    expect(spy).toHaveBeenCalled();
    expect(wrapper.vm.counter).toBe(mockedData);

    spy.mockRestore();
    spyInc.mockRestore();
  });

  test('should decrement counter correctly', async () => {
    const mockedData = 1;
    const mocketInitData = 2;
    const spyInc = jest.spyOn(service, 'getCounter').mockImplementation(() => {
      return mocketInitData;
    });

    const spy = jest
      .spyOn(service, 'decrementCounter')
      .mockImplementation(() => {
        return mockedData;
      });

    const wrapper = mount(Home);
    await flushPromises();
    await wrapper.vm.decrementCounterImpl();
    expect(spy).toHaveBeenCalled();
    expect(wrapper.vm.counter).toBe(mockedData);

    spy.mockRestore();
    spyInc.mockRestore();
  });

  test('should reset counter correctly', async () => {
    const mockedData = 3;
    const mocketInitData = 0;
    const spyInc = jest.spyOn(service, 'getCounter').mockImplementation(() => {
      return mocketInitData;
    });

    const spy = jest.spyOn(service, 'resetCounter').mockImplementation(() => {
      return mockedData;
    });

    const wrapper = mount(Home);
    await flushPromises();
    await wrapper.vm.resetCounterImpl();
    expect(spy).toHaveBeenCalled();
    expect(wrapper.vm.counter).toBe(mockedData);

    spy.mockRestore();
    spyInc.mockRestore();
  });
});
