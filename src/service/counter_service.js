import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localhost:3000/api/v1/counter',
  timeout: 1000,
  adapter: require('axios/lib/adapters/http'),
});

async function getCounter() {
  try {
    const response = await instance.get('/');
    if (response.data != null) {
      return response.data.count;
    }
  } catch (error) {
    console.log(error);
  }
}
async function incrementCounter() {
  try {
    const response = await instance.post('/increment');
    if (response.data != null) {
      return response.data.count;
    }
  } catch (error) {
    console.log(error);
  }
}
async function decrementCounter() {
  try {
    const response = await instance.post('/decrement');
    if (response.data != null) {
      return response.data.count;
    }
  } catch (error) {
    console.log(error);
  }
}
async function resetCounter() {
  try {
    const response = await instance.post('/reset');
    if (response.data != null) {
      return response.data.count;
    }
  } catch (error) {
    console.log(error);
  }
}

export { getCounter, incrementCounter, decrementCounter, resetCounter };
